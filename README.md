# Ressources Introductives pour CTF

## Challenges:

 - [Over The Wire](http://overthewire.org/wargames/)

	Pleins de challenges dans divers catégories; la difficulté augmente à chaque niveau
	Natas pour du web, Krypton pour crypto, Leviathan pour de l'exploitation linux
 - [CTF AGEEI](https://ctf.ageei.uqam.ca/challenges)

	Le CTF de l'asso; les premiers challenges de Web, Crypto, Stégano et Forensics sont plutôt approchables.
 - [Pentester Lab](https://www.pentesterlab.com/exercises/)

	Pleins de VMs vulnérables avec des explications étape par étape de comment les exploiter. Y'en a qui sont payantes ou trop spécifiques, mais celles Web For Pentester et SQLi sont très bien
 - [Exploit-Exercises](https://exploit-exercises.lains.space/)

	VM vulnérables contenant chacune une suite de challenges en ordre croissant de difficulté en partant du cas le plus simple. Couvre de l'exploitation binaire et de l'exploitation Linux
 - [RingZer0 Team CTF](https://ringzer0ctf.com/)

	Un site Québecois de challenges, allant du très facile au ridiculement difficile. Y'a très peu d'indices et de guidage, donc je ne recommande pas de commencer par ça

## Tools:

 - [GEF](https://gef.readthedocs.io/en/master/)

	Add on pour GDB qui aide pour le reverse engineering et le développement d'exploits
 - Binwalk et Foremost

	Servent à trouver des fichiers cachés dans d'autres fichiers pour le Forensics
 - Objdump (par défaut dans linux normalement)

	Pour dumper les infos et l'assembleur d'un binaire
 - [Tachyon](https://github.com/delvelabs/tachyon)

	Pour trouver des pages ou dossiers cachés sur des sites web
 - John The Ripper ou Hashcat

	Pour cracker des hash de mots de passe (ou d'autre chose)

## Autres

 - [Montréhack](https://montrehack.ca/)

	Rencontre mensuelle de sécurité informatique à Montréal. Se donne dans un format workshop; la personne qui présente donne des indices, répond aux questions et montre les solutions à la fin de la soirée. C'est une bonne place pour rencontrer d'autres personnes intéressées par la sécurité informatique et apprendre d'eux.
 - [LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)

	Un pentester allemand qui fait des vidéos en anglais et qui explique bien les concepts. Les playlist Intro to Binary Exploitation et Intro to web hacking sont les intéressantes pour commencer.
 - Hacking The Art Of Exploitation

	Un livre assez vieux et dense, mais il couvre bien les concepts et techniques de base.
 - [CTFTime](https://ctftime.org/)

	Une liste des CTFs à venir et passé avec des write-ups de challenges. Des fois, y'a rien de mieux pour comprendre une vulnérabilité ou un challenge que de lire comment quelqu'un a réussi quelque choise de similaire.
 - [Hacker 101](https://github.com/Hacker0x01/hacker101)

	Intro aux vulnérabilités communes en web par HackerOne et comment les trouver et exploiter. Y'a des vidéos, des textes et des exercices. Les explications sont bonne spour comprendre les bases, mais les exercices ne sont pas très bon selon moi.

Évidemment, il en existe beaucoup plus, mais ceci représente un bon point d'entrée. Pour plus, voir [CTF Katana](https://github.com/JohnHammond/ctf-katana) ou [CTF Field Guide](https://trailofbits.github.io/ctf/)
