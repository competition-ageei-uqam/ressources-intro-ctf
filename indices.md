% Indices - Atelier Intro CTF
% Barberousse
% 15 Novembre 2018

# Flag format : `UQAM{[A-Za-z0-9\_,\*;\'\$\%]+}`

## 1337 h4x0r reverse engineering / Way Too Long To Reverse

### Indice 1

`man strings`

---

### Indice 2

`grep` that flag format

## Deal With The Devil - Stéganographie

### Indice 1

Hmmm, c'est louche les métadonnées disent Created with photoshop, mais dans strings y'a created with GIMP plus loin dans le fichier.

## Commonwealth Alliance for Computer Assisted Surveillance - Web

### Indice 1

`robots.txt`?

---

### Indice 2

On peut changer les paramètres d'une requête sans passer par un formulaire.

## Secure Uncrackable Blockchain Zip Encryption

### Indice 1

'Do not use the MD5 algorithm for security related purposes.' - Manpage de `md5sum`

---

### Indice 2

Il existe des bases de données de hash communs

## Wiresharknado

### Indice 1

On mentionne qu'un fichier a été téléchargé, ce qu'on cherche risque donc d'être dans un paquet d'un protocole de couche application (FTP, HTTP, POP3, IMAP, DNS, etc.)

---

### Indice 2

Regardez le MIME type des réponses

## 64 bits Cryptominer 5.0

### Indice 1

Le fichier est un ELF 64-bit little endian, c'est à dire un exécutable Linux 64bits où le byte de poids le plus faible est stocké en premier pour les données. (Ça semble être à l'envers pour un humain)

Ex: `0xA0B70708` est stocké comme `08 07 B7 A0`

---

### Indice 2

Le range des caractères ASCII affichables va de 0x20 à 0x7E

---

### Indice 3

`RCX` est un registre et l'instruction `PUSH` met un élément sur la pile

## TranEquiUnionFax

### Indice 1

Regardez le code source de la page d'accueil

---

### Indice 2

Il y a des valeurs hardcodées dans la validation au login

---

### Indice 3

L'utilisateur peut contrôler et modifier ses cookies

---

### Indice 3

Une erreur de logique s'est glissée dans le choix des données à afficher dans la page admin
